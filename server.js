var express = require('express');
var app = express();

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
    res.render('index.jade', { title: 'title' });
});


  var user1 = {
    id: 1,
    name: "Человек №3596941",
    status: "Прежде чем действовать, надо понять",
    pass: "123",
    email: "mail@mail.ru",
    check: true
  };


app.get('/user', function (req, res) {
	console.log('I received a GET request');
	res.json(user1);
});

app.listen(3000);
console.log("Server running on port 3000");