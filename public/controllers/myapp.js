angular.module('myApp')
  .controller('AppCtrl', ['$scope', '$http', function($scope, $http) {


    $http.get('/user').success(function(response) {
      console.log("I got the data user");
      $scope.user = response;
    });

    $scope.$watch('form_fields.password', function(new_pass) {
      if( $scope.isPasswordOk() ) {
        $scope.form.password.$setValidity('min', true);
      } else {
        $scope.form.password.$setValidity('min', false);
      }
    });

    $scope.$watch('form_fields.password_confirm', function(new_pass) {
      if($scope.isConfirmPasswordMatch() && $scope.isPasswordOk() ) {
        $scope.form.password_confirm.$setValidity('match', true);
      } else {
        $scope.form.password_confirm.$setValidity('match', false);
      }
    });

    $scope.isPasswordOk = function() {
      if( $scope.form_fields != undefined)
      {  
        return $scope.form_fields.password.length > 5;
      }
      else{
        return false;
      }
        
    }

    $scope.isConfirmPasswordMatch = function() {
      //console.log($scope.form.password_confirm.$viewValue);
      if( $scope.form_fields != undefined)
      {
        return $scope.form_fields.password == $scope.form_fields.password_confirm;
      }
      else{
        return false;
      }
    }

    $scope.submit_form = function () {
      if ( $scope.form.$valid ) {
        //alert('submit');
        $scope.user.status = $scope.form_fields.status;
        clear_form();
      }
    }

    function clear_form () {
      $scope.form_fields.pass = "";
      $scope.form_fields.confirm_pass = "";
      $scope.form_fields.status = "";
      $scope.form_fields.email = "";
      $scope.form_fields.check = false;
      $scope.showForm = false;
    }

    $scope.isInputValid = function(input) {
      return input.$dirty && input.$valid;
    }

    $scope.isInputInvalid = function(input) {
      return input.$dirty && input.$invalid;
    }


}]);﻿