// include the required packages. 
var gulp = require('gulp');
var stylus = require('gulp-stylus');
var gutil = require('gulp-util');



gulp.task('stylus', function () {
    return gulp.src('./source/stylus/styles.styl')
        .pipe(stylus({
            compress: true
        }))
        .on('error', function(err) {
            // Handle stylus errors, but do not stop watch task
            gutil.log(gutil.colors.red.bold('[Stylus error]'));
            gutil.log(gutil.colors.bgRed('filename:') +' '+ err.filename);
            gutil.log(gutil.colors.bgRed('lineNumber:') +' '+ err.lineNumber);
            gutil.log(gutil.colors.bgRed('extract:') +' '+ err.extract.join(' '));
            this.emit('end');
        })
        .pipe(gulp.dest('./public/dist/css'))
});

gulp.task('watch', () => {
    gulp.watch('./source/stylus/*.styl', ['stylus']);
});

gulp.task('default', [ 'watch']);